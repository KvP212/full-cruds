import app from './app'
import sequelize from './database/mysql-db'

sequelize
  .authenticate()
  .then( () => console.log('Connection has been established successfully.'))
  .catch( err => console.log('Unable to connect to the database:', err))

const main = () => {
  app.listen(app.get('port'))
  console.log(`Run on ${app.get('port')}`)
}

main()

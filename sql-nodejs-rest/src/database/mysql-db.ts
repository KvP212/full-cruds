import {Sequelize} from 'sequelize'

const sequelize = new Sequelize(
  'marketbpqs',
  'admin',
  process.env.MYSQLPASS,
  {
    host: 'localhost',
    dialect: 'mysql'
  }
)

export default sequelize

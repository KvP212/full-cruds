import express, {Application}  from 'express'
import morgan from 'morgan'
const app: Application = express()

/**
 * Settings
*/
app.set('port', process.env.PORT || 4040)

//middlewares
app.use(morgan('dev'))
app.use(express.json())

//Routes

export default app
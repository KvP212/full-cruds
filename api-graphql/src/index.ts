import "reflect-metadata";
import { startGraphql } from './app'
import { connect } from './config/typeorm'

const main = async () => {
  await connect()

  const app = await startGraphql()
  app.set('port', process.env.PORT || 4040)
  app.listen( app.get('port') )
  console.log(`Run on ${app.get('port')}`)

}

main();

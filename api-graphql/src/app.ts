import express from 'express'
import { ApolloServer } from 'apollo-server-express'

// import { GraphQLServer } from 'graphql-yoga'
import { buildSchema } from 'type-graphql'

//type Definitions
import { PingResolver } from './graphql/resolvers/Hello'
import { ProductResolver } from './graphql/resolvers/ProductResolver'


export async function startGraphql() {

  const app = express()
  const server = new ApolloServer({
    schema: await buildSchema({
      resolvers: [PingResolver, ProductResolver],
      validate: false
    }),
    context: ({req, res}) => ({req, res})
  })

  server.applyMiddleware({ app, path: '/graphql' })

  return app
}

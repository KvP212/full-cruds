import { createConnection } from 'typeorm'
import path from 'path'

export async function connect() {
  try {

    await createConnection({
      type: "mysql",
      host: 'localhost',
      username: 'admin',
      password: process.env.MYSQLPASS,
      database: 'graphqlts',
      entities: [
        path.join(__dirname, '../entity/**/**.js')
      ],
      synchronize: true
    })

    console.log('Conectado a db')

  } catch (e) {
    console.log('Error en la conexion a db')
  }

}

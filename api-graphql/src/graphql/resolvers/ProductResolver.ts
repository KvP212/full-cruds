import { Resolver, Query, Mutation, Arg, Field, InputType, Int } from 'type-graphql'
import { Product } from '../../entity/Product'

@InputType()
class ProductInput {

  @Field( () => String, {nullable: true })
  name?: string

  @Field( () => Int , { nullable: true })
  quantity?: number
}

@Resolver()
export class ProductResolver {

  @Mutation( () => Product)
  async createProduct(
    @Arg('variables') variables: ProductInput
  ) {

    return await Product.create(variables).save()

  }

  @Mutation(() => Boolean)
  async deleteProduct(
    @Arg('id', () => Int) id: number
  ) {

    await Product.delete(id)
    return true

  }

  @Mutation(() => Boolean)
  async updateProduct(
    @Arg('id', () => Int) id: number,
    @Arg('fields', () => ProductInput) fields: ProductInput
  ) {

    await Product.update({id}, fields)
    return true

  }

  @Query( () => [Product])
  async getProduct() {
    return await Product.find()
  }
}
